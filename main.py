import datetime

from kivy import Config
from kivy.app import App
from kivy.properties import ObjectProperty, Clock
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager, FadeTransition
from kivy.uix.spinner import Spinner, SpinnerOption

from Utils.StringConfig import StringConfig
from Utils.ValidateLogin import ValidateLogin

Config.set('graphics', 'resizable', False)

screenManager = ScreenManager(transition=FadeTransition())


class LoginScreen(Screen):
    def login(self):

        username = self.ids.ti_username.text
        password = self.ids.ti_password.text

        if ValidateLogin.validate(self, username, password) is True:

            ValidateLogin.clearLoginForm(self)

            screenManager.add_widget(MainScreen())
            self.manager.switch_to(MainScreen())

            print('Valid')
        else:
            print('Invalid')

            displayPopUp(self, StringConfig.popup_login_title, ValidateLogin.fetchLoginError(self))

            ValidateLogin.clearLoginForm(self)

    def cancel(self):
        screenManager.add_widget(DisplayScreen(''))
        screenManager.switch_to(DisplayScreen(''))


class MainScreen(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.setDisplayName()

    # literally sets display name
    def setDisplayName(self):
        self.ids.lbl_main_name.text = StringConfig.user_01[2]

    # literally gets display name
    def getDisplayName(self):
        return self.ids.lbl_main_name.text

    # literally gets spinner value
    def getSpinnerValue(self):
        return self.ids.customSpinner.text

    # this function set the default spinner text
    def onSpinnerPress(self):
        self.ids.customSpinner.text = 'STATUS'

    def update(self):

        status = self.getSpinnerValue()

        if status != 'STATUS':

            displayConfirmationDialog('display_screen', status, StringConfig.popup_update_status_title)

        else:

            displayPopUp(self, StringConfig.popup_update_status_failed_title,
                         StringConfig.popup_update_status_failed_message)


class DisplayScreen(Screen):
    time = ObjectProperty(None)

    def __init__(self, status):
        super().__init__()

        self.status = status
        self.setStatus(self.status)
        self.setDisplayName()

        Clock.schedule_interval(self.smartStatusLol, 1)

    def setDisplayName(self):

        self.ids.lbl_display_name.text = StringConfig.user_01[2]

    def setStatus(self, status):
        print(status)
        self.ids.lbl_display_status.text = status

    def update(self):
        displayConfirmationDialog('login_screen', '', StringConfig.popup_update_status_title)

    def smartStatusLol(self, *args):
        self.time = datetime.datetime.now()

        currentTime = str(self.time.strftime('%I' + ":" + "%M" + ":" + "%S" + " %p"))
        if currentTime == '10:00:00 PM':
            self.setStatus('OUT')



def displayConfirmationDialog(nextScreen, status, dialogTitle):
    confirmationDialog = ConfirmationDialog(nextScreen, status, dialogTitle)
    confirmationDialog.setMessage()
    confirmationDialog.open()


class ConfirmationDialog(Popup):
    manager = screenManager

    def __init__(self, nextScreen, status, dialogTitle):
        super().__init__()

        self.title = dialogTitle
        self.nextScreen = nextScreen
        self.status = status

    def setMessage(self):
        self.ids.lbl_confirmation.text = StringConfig.popup_update_status_message

    def setNoButton(self):
        self.dismiss()

    def setYesButton(self):
        self.dismiss()

        if self.nextScreen == 'login_screen':

            screenManager.add_widget(LoginScreen())
            self.manager.switch_to(LoginScreen())

        elif self.nextScreen == 'display_screen':

            screenManager.add_widget(DisplayScreen(self.status))
            self.manager.switch_to(DisplayScreen(self.status))


def displayPopUp(self, errorTitle, errorMessage):
    popup = CustomPopup(title=errorTitle)
    popup.setErrorMessage(errorMessage)
    popup.open()


# class for custom pop up
class CustomPopup(Popup):
    def setErrorMessage(self, someErrorMessage):
        self.ids.lbl_popup.text = someErrorMessage

    def closePopup(self):
        self.dismiss()


# class for custom spinner
class CustomSpinner(Spinner):
    pass


class AppHeader(AnchorLayout):
    pass


class AppBody(BoxLayout):
    pass


class DateTimeDisplay(BoxLayout):
    time = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(DateTimeDisplay, self).__init__(**kwargs)

        Clock.schedule_interval(self.on_time, 1 / 60)

    def on_time(self, *args):
        self.time = datetime.datetime.now()
        self.ids.time.text = str(self.time.strftime('%I' + ":" + "%M" + ":" + "%S" + " %p"))
        self.ids.date.text = str(self.time.strftime('%B ' + '%d' + ', ' + '%Y'))


class MainApp(App):
    def build(self):
        #screenManager.add_widget(DisplayScreen('In Official Business'))
        # screenManager.add_widget(MainScreen())
        screenManager.add_widget(LoginScreen())

        return screenManager


if __name__ == '__main__':
    MainApp().run()
