class StringConfig:

    user_01 = ['ADO', '1234', 'Engr. Remedios G. Ado']


    # default_user = 'ADO'
    #
    # default_name = 'Engr. Remedios G. Ado'

    #default_pass = '1'

    popup_login_title = 'Login Failed!'

    popup_login_empty_message = 'All fields are required.'

    popup_login_invalid_message = 'Invalid credentials.'

    popup_update_status_title = 'Update Status?'

    popup_update_status_message = 'Do you really want to update your status?'

    popup_update_status_failed_title = 'Update status failed!'

    popup_update_status_failed_message = 'Please update your status'
