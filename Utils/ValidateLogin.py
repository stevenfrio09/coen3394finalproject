# this class validates the login form
from Utils.StringConfig import StringConfig


class ValidateLogin:
    message = ''

    def validate(self, username, password):
        if username == StringConfig.user_01[0] and password == StringConfig.user_01[1]:
            return True
        else:
            ValidateLogin.handleLoginErrors(self, password, username)

            return False

    def handleLoginErrors(self, password, username):
        if len(username) == 0 or len(password) == 0:

            self.message = StringConfig.popup_login_empty_message

        elif username != StringConfig.user_01[0] or password != StringConfig.user_01[1]:

            self.message = StringConfig.popup_login_invalid_message

    def fetchLoginError(self):
        return self.message

    def clearLoginForm(self):
        self.ids.ti_username.text = ''
        self.ids.ti_password.text = ''
